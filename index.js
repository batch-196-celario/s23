console.log("Hi There!");



let PokemonTrainer ={

	name: "Red",
	Age: 15,
	pokemon: ["Magikarp","Bidoof","Zigzagoon","Sentret"],
	friends: {
		Kanto: ["blue","green"],
		Johto: ["Gold","Silver"]
	},
	talk: function(){
		console.log("Magikarp!, I choose you!");
	}

};
console.log(PokemonTrainer);

console.log("Result of dot notation:")
console.log(PokemonTrainer.name);
let Pokemon = PokemonTrainer.pokemon;
console.log("Result of square bracket notation:")
console.log(Pokemon);
console.log("Result of talk method:")
PokemonTrainer.talk();

function createPokemon(name, level){
	this.Name = name;
	this.Level = level;
	this.Health = 3 * level;
	this.Attack = 1.5 * level;
	this.tackle = function(target) {
		let attack = target.Health - this.Attack;
		console.log(this.Name + " tackled " + target.Name);
		console.log(target.Name + " health is reduced to " + attack);
		 if (attack < 0) {
		 	target.faint()
		 }
	}
    this.faint = function(){
    	console.log(this.Name + " Has fainted!");
    }
}

let Pokemon1 = new createPokemon("Pikachu", 12)
let Pokemon2 = new createPokemon("geodude", 8);
let Pokemon3 = new createPokemon("Mewtwo", 100);

console.log(Pokemon1);
console.log(Pokemon2);
console.log(Pokemon3);

Pokemon1.tackle(Pokemon3);
Pokemon1.tackle(Pokemon2);
Pokemon3.tackle(Pokemon1);

//Pokemon2.faint();
